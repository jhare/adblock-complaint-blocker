Find those tasty DOM elements with the tears of online publishers begging for you to whitelist them on your AdBlocker. Get fucked.

## File breakdown

* `spiteful.js` - the algorithm and methods that define how this framework filters URLs and links callbacks.
* `lib/**/*.js` - various helper logic files for `spiteful.js`
* `extensions/chrome` - the guts that make the Chrome extension work
* `extensions/firefox` - the guts that make the Firefox extension work
* `build` - this grabs all of the above to actually prouduce the `dist` artifact
* `dist` should be the result of a successful build